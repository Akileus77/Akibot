# Gems
::RBNACL_LIBSODIUM_GEM_LIB_PATH = "C:/Ruby24/libsodium.dll"
require 'bundler/setup'
require 'discordrb'
require 'yaml'
require 'lol'
require 'httparty'
require 'colorize'
require 'unidecoder'
require 'myanimelist'
# The main bot module.
module Bot
  # Load non-Discordrb modules
  Dir['src/modules/*.rb'].each { |mod| load mod }

  # Bot configuration
  CONFIG = OpenStruct.new YAML.load_file 'data/config.yaml'
  # Create the bot.
  # The bot is created as a constant, so that you
  # can access the cache anywhere.
  BOT = Discordrb::Commands::CommandBot.new(client_id: CONFIG.client_id,
                                            token: CONFIG.token,
                                            prefix: CONFIG.prefix)

  # Discord commands
  module DiscordCommands; end
  Dir['src/modules/commands/*.rb'].each { |mod| load mod }
  DiscordCommands.constants.each do |mod|
    BOT.include! DiscordCommands.const_get mod
  end

  # Discord events
  module DiscordEvents; end
  Dir['src/modules/events/*.rb'].each { |mod| load mod }
  DiscordEvents.constants.each do |mod|
    BOT.include! DiscordEvents.const_get mod
  end

  # Run the bot
  BOT.run
end

client = Lol::Client.new "RGAPI-e2d4eb17-4fd2-4a19-8495-2e33c3f65ae0", {region: "tr"}
# NEW! You can cache requests using Redis now
# ttl defaults to 900
client = Lol::Client.new "RGAPI-e2d4eb17-4fd2-4a19-8495-2e33c3f65ae0", {region: "TR", redis: "redis://localhost:6379", ttl: 900}
# Available Requests
client.champion
# => Lol::ChampionRequest
client.game
# => Lol::GameRequest
client.league
# => Lol::LeagueRequest
client.stats
# => Lol::StatsRequest
client.summoner
# => Lol::SummonerRequest
client.team
# => Lol::TeamRequest

# Available methods for each request type
client.champion.get
# => Lol::Champion

client.game.recent(summoner_id)
# => Lol::Game

client.league.get(summoner_id)
# => Lol::League

client.stats.summary(summoner_id)
# => Lol::SummaryStats
client.stats.ranked(summoner_id)
# => Lol::RankedStats

client.summoner.masteries(summoner_id)
# => [Lol::Masterypage]
client.summoner.runes(summoner_id)
# => [Lol::Runepage]
client.summoner.by_name(name)
# => Lol::Summoner
client.summoner.get(summoner_id)
# => Lol::Summoner
client.summoner.name(summoner_ids)
# => [Hash]

client.team.get(summoner_id)
# => Array

MyAnimeList.verify_credentials
