module Bot
  module DiscordCommands
    module Replik
      extend Discordrb::Commands::CommandContainer
          command :Replik do |event|
            json = JSON.parse(File.read("data/replik.json"))
            son = json['replik']
            i = rand(0..(son.size - 1))
            r1 = son[i]
            event.send r1
          end
        end
      end
    end
