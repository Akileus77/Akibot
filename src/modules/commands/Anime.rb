=begin
module Bot
  module DiscordCommands
    module Anime
      extend Discordrb::Commands::CommandContainer
      command :Anime do |event, args|

        anime = URI.escape(args)
        url = 'https://myanimelist.net/api/anime/search.xml?q='+ anime +''
        response = HTTParty.get(url)
        body = JSON.parse(response.body)
        animeisim = body["title"]
        event.send ' #{animeisim}'
      end
    end
  end
end
=end
